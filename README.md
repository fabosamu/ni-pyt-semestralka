# Text2Bank application

Winning idea of Profinit Innovation Program

The application's main purpose is to collect user data - queries and simple classifying of queries.
This should work like a potential text assistant for internet banking - The topic of Profinit PoC Innovation Program


## Development

Below is a tutorial on how to prepare and run the app in development mode. You might want to edit ``.env.dev``.

1. Install and start Docker
2. Open terminal / command line, change directory to text2bank repository and run:
```   
    docker-compose up -d --build
```
3. On `localhost:5001/` you will see the login page.
4. To "turn it off", run:
```
    docker-compose down -v
```
    

This is the most basic guide on how to run the app in development mode. Docker will take care of the database and flask app.

Everything important is located in ``docker-compose.yaml``

For the process of development and deployment, I used a tutorial by [Michael Herman - testdriven.io](https://testdriven.io/blog/dockerizing-flask-with-postgres-gunicorn-and-nginx/).
For user's login-signup-dashboard process I used tutorial by [Todd Birchard - HackarsAndSlackers](https://hackersandslackers.com/flask-login-user-authentication/).

If you want to play around with the database, run:
```
    docker-compose exec db psql --username=text2bank --dbname=text2bank_dev
    docker volume inspect flask-on-docker_postgres_data
````


## Production


To run [Gunicorn](https://gunicorn.org/) and the whole app with postgres in a production mode, edit ``.env.prod`` and ``.env.prod.db`` accordingly.
Everything important is located in ``docker-compose.prod.yaml``

Dont' forget to set correct execution rights of ``entrypoint.prod.sh`` file ::
```
    chmod +x services/web/entrypoint.prod.sh
```

**Make sure ``entrypoint.sh`` has correct unix line endings ``\n``. You can use ``dos2unix``.**

### Up and Running

To deploy the production version, run::
```
    docker-compose -f docker-compose.prod.yaml up -d --build
    docker-compose -f docker-compose.prod.yaml exec web python manage.py create_db # ATTENTION, this will delete the DB and create a fresh one.
```

### Versioning

Each time a new version is pushed, this should be modified in the database. Run:
```
    docker-compose -f docker-compose.prod.yaml exec web python manage.py version --sha SHA --tag TAG --description DESCR
```
If you have set the git tag and description accordingly, you can get it from git:
```
    docker-compose -f docker-compose.prod.yaml exec web python manage.py version -s $(git rev-parse --short HEAD) -t $(git describe --tags) -d $(git tag -n1)
```

**Current time and recipe will be filled for the newly created Version.**

### Database

If you want to play around in the database, run:
```
    docker-compose -f docker-compose.prod.yaml exec db psql --username=text2bank --dbname=text2bank_prod
```

**The app is NOT using ngnix proxy (yet). Deploy only within the company's environment.**


## Documentation

Use the tutorial by [Nauc Se Python](<https://naucse.python.cz/2020/nipyt-zima/intro/docs/)

To create these html files created by [Sphinx](https://www.sphinx-doc.org/en/master/), run:
```
    cd services/web/docs
    make html
```
When updating the modules, run:
```
    cd services/web
    sphinx-apidoc -o docs mymodule
```

``index.html`` is located in ``services/web/docs/_build/html``


## Testing


This application uses [pytest](https://docs.pytest.org/en/latest/index.html)
framework with [tox](https://tox.readthedocs.io/en/latest/) to automate testing.

To run all tests, install tox:
```
    pip install tox
```
change directory to ``text2bank/services/web`` and run::
```
    tox
```


## Contact:

* petr.pascenko@profinit.eu
* pavel.svoboda@profinit.eu
* samuel.fabo@profinit.eu
