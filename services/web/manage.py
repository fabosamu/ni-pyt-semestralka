import click
from flask.cli import FlaskGroup

from text2bank_app import create_app, db
from text2bank_app.db_helpers import insert_dummy_responses, insert_dummy_responses_anonym
from text2bank_app.models import Version

app = create_app()
cli = FlaskGroup(app)


@cli.command("version")
@click.option('-s', '--sha', default='Not Set',
              help='SHA of current commit. Use `git rev-parse --short HEAD\'')
@click.option('-t', '--tag', default='Not Set',
              help='Current version number. Use e.g., `git describe --tags\'')
@click.option('-d', '--description', default='Not Set',
              help='Current tag description. Use e.g., `git tag -n1\'')
def insert_version(sha, tag, description):
    """
    CLI command for inserting current version into the DB.

    Parameters
    ----------
    sha
        hash of current commit
    tag
        Current version number
    description
        Current tag description
    """
    with open(app.config['RECIPE_PATH']) as f:
        recipe_text = f.read()

    app.logger.info(f'Inserting into VERSION: '
                    f'commit_hash: {sha}, '
                    f'version_number: {tag}, '
                    f'description: {description}')
    version = Version(
        commit_hash=sha,
        version_number=tag,
        description=description,
        recipe_text=recipe_text,
    )

    db.session.add(version)
    db.session.commit()
    app.config['version_tag'] = tag


@cli.command("create_db")
def create_db():
    db.drop_all()

    # Create Database Models
    db.create_all()

    if app.config['FLASK_ENV'] == 'development':
        insert_dummy_responses_anonym()


@app.context_processor
def context_processor():
    tag = app.config.get('version_tag')
    return dict(version_tag=tag) if tag else dict(version_tag='')


if __name__ == "__main__":
    cli()
