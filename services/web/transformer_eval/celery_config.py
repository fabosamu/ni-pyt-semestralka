import os
from pathlib import Path

broker_url = os.getenv('RABBIT_URL')
if not broker_url:
    raise RuntimeError('RABBIT_URL environment variable needed.')
result_backend = 'rpc'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']

worker_max_memory_per_child = 1000 #* 1000  # in KiB, 1GB
max_tasks_per_child = 10
find_task_interval = 60

celery_load_ruian = os.getenv('CELERY_LOAD_RUIAN', '0')

transformer_path = './transformer_eval/resources/robeczech-best'

knowledge_base_path = './transformer_eval/resources/merged-2022-03-24.csv'

# fasttext_path: 'transformer_eval/resources/cc.cs.300.bin'
fasttext_path = '/fastText/cc.cs.300.bin'

stop_words_path = 'nb_query_eval/resources/stopwords.txt'
