from pathlib import Path
import pandas as pd
import os

from celery import Celery
from celery.utils.log import get_task_logger
from nb_query_eval import Tokenizer, helpers
from . import ClassifierOOD
from . import Evaluator
import fasttext

import nltk
nltk.download('punkt')

logger = get_task_logger(__name__)

celery_app = Celery('wks')
# celery_app.config_from_object('./celery_config')

class Config:
    broker_url = os.getenv('RABBIT_URL')
    if not broker_url:
        raise RuntimeError('RABBIT_URL environment variable needed.')
    result_backend = 'rpc'

    task_serializer = 'pickle'
    result_serializer = 'pickle'
    accept_content = ['pickle']

    # worker_max_memory_per_child = 1000 #* 1000  # in KiB, 1GB
    max_tasks_per_child = 10
    find_task_interval = 60

    celery_load_ruian = os.getenv('CELERY_LOAD_RUIAN', '0')

    transformer_path = './transformer_eval/resources/robeczech-best'

    knowledge_base_path = './transformer_eval/resources/merged-2022-03-24.csv'

    # fasttext_path: 'transformer_eval/resources/cc.cs.300.bin'
    fasttext_path = 'cc.cs.300.bin'

    stop_words_path = 'nb_query_eval/resources/stopwords.txt'


    if os.getenv('LOAD_FT_TFMR', '0') == '1':
        tfmr_eval = Evaluator(transformer_path)

        kb = pd.read_csv(knowledge_base_path)
        tokenizer = Tokenizer(helpers.read_stop_words(stop_words_path))
        ood = ClassifierOOD(
            knowledge_base=kb['text_cz'],
            kb_categories=kb['kategorie'],
            # fasttext_obj=ft,
            fasttext_path=fasttext_path,
            tokenizer=tokenizer)


celery_app.config_from_object(Config)


@celery_app.task
def get_kb_max_simil(text: str):
    ood = celery_app.conf['ood']
    max_simil = ood.get_kb_similarity([text])[0]['max_simil']
    return max_simil


@celery_app.task
def predict(text: str):
    tfmr = celery_app.conf['tfmr_eval']
    predictions = tfmr.predict(text, sort_results=True)
    return predictions