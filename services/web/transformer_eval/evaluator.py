from pathlib import Path
from transformers import AutoModelForSequenceClassification, BertTokenizer, AutoConfig, RobertaTokenizer
from torch.nn import Softmax

# TODO: FUJ FUJ FUJ!!!

cat2label = {'aktivovat moji kartu': 0, 'bezkontaktní platba nefunguje': 1, 'karta nefunguje': 2,
             'karta spolknuta': 3, 'zablokovaný pin': 4, 'karta zneužita': 5, 'limity jednorázových karet': 6,
             'délka doby převodu ': 7, 'nesprávná částka přijaté hotovosti z bankomatu': 8, 'neúspěšný převod': 9,
             'odhad času doručení karty': 10, 'odmítnuta platba kartou': 11, 'odmítnutý převod': 12,
             'odmítnutý výběr hotovosti': 13, 'ověřit zdroj financí': 14, 'platba inkasem nebyla rozpoznána': 15,
             'nerozpoznávám platbu kartou': 16, 'platba kartou špatný směnný kurz': 17, 'platnost karty brzy skončí': 18,
             'podpora bankomatu': 19, 'podpora fiat měny': 20, 'podpora země': 21, 'poplatek za platbu kartou': 22,
             'poplatek za výběr hotovosti': 23, 'požadovat vrácení peněz': 24, 'příjemce neobdržel převod': 25,
             'přijímání karet': 26, 'příchod karty': 27, 'příjemce převodu není povolen': 28, 'poplatek navíc ve výpisu': 29,
             'směnný kurz': 30, 'transakce účtována dvakrát': 31, 'ukončit účet': 32, 'upravit osobní údaje': 33,
             'virtuální karta nefunguje': 34, 'vrácení peněz se nezobrazuje': 35, 'vrácena platba kartou?': 36, 'změnit pin': 37,
             'visa nebo mastercard': 38, 'věkový limit': 39, 'zapomenuté přístupové heslo nebo kód': 40, 'zrušit převod': 41,
             'ztracená nebo odcizená karta': 42, 'ztracený nebo odcizený telefon': 43, 'získat jednorázovou virtuální kartu': 44,
             'získání náhradní karty': 45, 'získání virtuální karty': 46, 'zůstatek není aktualizován po bankovním převodu': 47,
             'zůstatek není aktualizován po šeku nebo vkladu hotovosti': 48, 'účtovaný poplatek za převod': 49, 'čekající platba kartou': 50,
             'čekající převod': 51, 'čekající výběr hotovosti': 52, 'špatný směnný kurz pro výběr hotovosti': 53, 'chci půjčit/kolik mi půjčíte': 54,
             'jednorázová platba tuzemská': 55, 'potřebuji mimořádnou splátku (hypotéky)': 56, 'potřebuji potvrzení o platbě': 57, 'sjednat hypotéku': 58,
             'sjednat schůzku na pobočce': 59, 'splátka kreditní karty': 60, 'svolení k inkasu a sipo': 61, 'trvalé platby': 62, 'výpisy': 63}


class Evaluator:

    def __init__(self, transformer_path: Path) -> None:
        # self.model, self.tokenizer = self.load_transformer(transformer_path)
        self.model, self.tokenizer = self.load_robeczech(transformer_path)
        self.action_names = cat2label.keys()

    def predict(self, text: str, sort_results=False):
        print('OBTAINING TOKENS...')
        tokens = self.tokenizer([text.capitalize()],
                                padding=True, truncation=True,
                                return_tensors='pt')
        print('OBTAINING PREDICTIONS...')
        output = self.model(**tokens)
        pred = Softmax(dim=1)(output.logits)[0].detach().tolist()

        # same format as in ensemble
        result = [(p, nam_lab[1], nam_lab[0])
               for p, nam_lab in zip(pred, cat2label.items())]
        if sort_results:
            result = sorted(result, key=lambda t: t[0], reverse=True)
        print('RETURNING RESULTS')
        return result

    def load_robeczech(self, robeczech_model):
        tokenizer = RobertaTokenizer.from_pretrained(robeczech_model)
        model = AutoModelForSequenceClassification.from_pretrained(robeczech_model)
    
        return model, tokenizer

    def load_transformer(self, transformer_path: Path):
        tokenizer = BertTokenizer.from_pretrained(transformer_path)
        model = AutoModelForSequenceClassification.from_pretrained(
            transformer_path)
        return model, tokenizer

