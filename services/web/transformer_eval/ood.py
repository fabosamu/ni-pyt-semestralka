import fasttext
from sklearn.metrics.pairwise import cosine_similarity
from pathlib import Path


class ClassifierOOD:

    def __init__(self, knowledge_base: list, kb_categories: list, fasttext_obj=None, fasttext_path: Path = None, tokenizer=None):
        self.knowledge_base = knowledge_base
        self.knowledge_base_cat = kb_categories
        if fasttext_path is None:
            self.ft = fasttext_obj
        elif fasttext_obj is None:
            self.ft = fasttext.load_model(fasttext_path)
        else:
            raise ValueError(
                'fasttext_obj xOR fasttext_path should be defined!')
        self.tkz = tokenizer
        self.kb_embs = self._embedding_pipeline(knowledge_base)

    def _tokenize(self, texts):
        if self.tkz is not None:
            return [' '.join([w for w, v in self.tkz.fit(t) if v == 'KW']) for t in texts]
        else:
            return texts

    def _ft_sent(self, texts):
        return [self.ft.get_sentence_vector(c) for c in texts]

    def _embedding_pipeline(self, texts: list):
        clean_texts = self._tokenize(texts)
        return self._ft_sent(clean_texts)

    def get_kb_similarity(self, to_verify: list):
        to_verify_emb = self._embedding_pipeline(to_verify)
        cs = cosine_similarity(self.kb_embs, to_verify_emb)
        return [
            dict(max_simil=cs[:, i].max(),
                 best_category=self.knowledge_base_cat[cs[:, i].argmax()])
            for i in range(len(to_verify))
        ]
