from setuptools import setup, find_packages

setup(
    name='text2bank',
    version='0.4',
    keywords='flask web naive bayes labelling text banking assistant',
    description='Text2bank - text assistant for internet banking',
    author='Samuel Fabo',
    author_email='samuel.fabo@profinit.eu',
    url='www.profinit.eu',
    license='Proprietary',
    packages=find_packages(),
    package_data={
        'text2bank_app': [
            'templates/*'
        ]
    },
    install_requires=[
        'Flask_SQLAlchemy==2.4.4',
        'click==7.1.2',
        'SQLAlchemy==1.3.20',
        'Flask_Login==0.5.0',
        'pandas==1.1.3',
        'pytest==6.1.2',
        'psycopg2-binary==2.8.4',
        'email-validator',
        'WTForms==2.3.3',
        'Flask_WTF==0.14.3',
        'jellyfish==0.8.2',
        'Werkzeug==1.0.1',
        'Flask==1.1.2',
        'nltk==3.5',
        'schwifty==2020.9.0',
        'numpy==1.19.0',
        'python-dotenv==0.15.0',
        'PyYAML==5.4.1',
        'gunicorn==20.0.4',
        'click>=6',
        'python-dotenv',
        'sphinx',
    ],
    classifiers=[
        'License :: Other/Proprietary License',
        'Environment :: Web Environment',
        'Framework :: Flask',
        'Intended Audience :: Users',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.8',
    ],
    python_requires='>=3.8',
)
