.. Text2bank documentation master file, created by
    sphinx-quickstart on Tue Dec 22 11:09:36 2020.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to Text2bank's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Guides:

   tutorials/Overview
   tutorials/Development

.. toctree::
   :maxdepth: 3
   :caption: Modules:

   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Contacts Us
===========

* petr.pascenko@profinit.eu
* pavel.svoboda@profinit.eu
* samuel.fabo@profinit.eu

