********************
Overview
********************


Text2bank - Text Banking Assistant
==============================================

TL;DR: Pilot query classifier and entity extractor made mainly to collect users' queries for potential text assistant. Implemented as web application with Flask

Banks are trying to make life with internet banking easier.
But maybe even if they try hard, some people might find it confusing
and making the UX easier and easier may change the way people look at new updates of the UX design.


Definitions
===========

* Naive Bayes (NB): classifier used when evaluating a query
* Bayes Factor (BF): multiplier of prior probability for each action.
* query: user's input to be parsed and analyzed, what he/she means.
* action names: target classes / target actions, you name it. These are the output of the prediction and will be displayed
* keyword matrix: matrix used by NB. Columns are action names and rows are keywords with corresponding BF


How can I see it working?
==========================

If you are working for PROFINIT:
--------------------------------

* Open `this IP address under gr500 VPN <http://172.16.200.22:5001/>`_ in your browser.
* You will see login page, so if you don't have an account, sign up.
* Then you will be redirected to the dashboard page
* simply insert your query and hit the submit button.

Now you should see the actions, which were predicted from your query. If you inputted things like:

* IBAN
* Account number (Czech format)
* Amount (and currency)

These should have been extracted too and visible on top.

If you wish to see TOP_N accuracy report, visit ``http://172.16.200.22:5001/report``, where this aggregation is present.

If you wish to see report for each query, visit ``http://172.16.200.22:5001/report_queries``.

.. note::

    If you are running your own instance, change the IP address to ``http://localhost:5001/``

If you are not working for PROFINIT:
------------------------------------

You need to create your own instance of this application. See :doc:`Development`


How does it work?
==============================================

TL;DR: Flask app for user input in dockerized environment with Naive Bayes as the backend logic.

The main purpose of this project is to obtain as much user data as possible from colleagues.
Therefore, frontend is implemented in a very simple way and right now also the backend.
In the background, there is :mod:`nb_query_eval` package, where all the "magic" happens.
Only a few regexes are implemented with some analytical matrix of keywords (keyword matrix) for Naive Bayes.

Ensemble
--------

For better accuracy, there is an ensemble of two (or more potential) models, the output of which can be weighted and contribute to the global accuracy for a given query.
Each extractor should implement :class:`nb_query_eval.ensemble.Evaluator` and as an input it should take tokens from :class:`nb_query_eval.ensemble.Tokenizer`.


Query Evaluator (QE)
--------------------

Right now there is :mod:`nb_query_eval.query_evaluator` model, which uses raw Naive Bayes classifier and :class:`nb_query_eval.ensemble.Tokenizer` using `nltk <https://www.nltk.org/>`_.
This model matches each keyword (KW) token with keyword from keyword matrix and uses corresponding Bayes Factor ( == 1) to compute the final accuracy.
QE just classifies the user's query and evaluates the probability of any action from the KW matrix.


Entity Recognizer
-----------------

From the user's query, we need to extract any entity (such as amount, IBAN, CZ account number).
The actual recognizer/extractor is implemented in :mod:`nb_query_eval.entity_recognizer` and uses regexes for extraction.
This extractor also works as a classifier of the user's query and contributes


Interaction Flow
================

.. figure:: text2bank-sketch-EN.png
    :align: center
    :alt: simple interaction flow diagram
    :figclass: align-center

    This diagram is depicting the possible interaction of user and Text2bank


Database Model
===============

.. figure:: text2bank-DB.png
    :align: center
    :alt: Database model
    :figclass: align-center

    Database model

