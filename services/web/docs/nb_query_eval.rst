nb\_query\_eval package
=======================

Submodules
----------

nb\_query\_eval.ensemble module
-------------------------------

.. automodule:: nb_query_eval.ensemble
   :members:
   :undoc-members:
   :show-inheritance:

nb\_query\_eval.entity\_recognizer module
-----------------------------------------

.. automodule:: nb_query_eval.entity_recognizer
   :members:
   :undoc-members:
   :show-inheritance:

nb\_query\_eval.helpers module
------------------------------

.. automodule:: nb_query_eval.helpers
   :members:
   :undoc-members:
   :show-inheritance:

nb\_query\_eval.query\_evaluator module
---------------------------------------

.. automodule:: nb_query_eval.query_evaluator
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nb_query_eval
   :members:
   :undoc-members:
   :show-inheritance:
