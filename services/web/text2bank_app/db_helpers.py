import numpy as np
import pandas as pd
from flask import current_app
from sqlalchemy import and_

from text2bank_app import db
from text2bank_app.models import Result, User, Query, Response


# def signup_user(name, email, password):
#     """
#     Inserts new user into the DB

#     Parameters
#     ----------
#     name
#     email
#     password

#     Returns
#     -------
#     :class:`text2bank_app.models.User`
#     """
#     user = User(
#         name=name,
#         email=email,
#     )
#     user.set_password(password)
#     db.session.add(user)
#     db.session.commit()  # Create new user
#     return user


def signup_user_anonym(name):
    user = User(
        name=name,
    )
    db.session.add(user)
    db.session.commit()  # Create new user
    return user


def add_query(query_input, user_id):
    """
    Adds user's input query into the DB

    Parameters
    ----------
    query_input: str
    user_id
        of current user
    Returns
    -------
    :class:`text2bank_app.models.Query`
    """
    current_app.logger.debug(f'adding {query_input} of '
                             f'user_id {user_id} to DB...')
    query = Query(
        input=query_input,
        user_id=user_id,
    )
    db.session.add(query)
    db.session.commit()
    current_app.logger.debug(f'adding {query_input} of '
                             f'user_id {user_id} to DB done!')
    return query


def write_results(query_id, proba_action):
    """
    Writes :class:`text2bank_app.models.Result` into the DB.

    Parameters
    ----------
    query_id
    proba_action: list
        actions and their estimated probabilities

    Returns
    -------
    list
        result dicts
    """
    results = []
    for i in range(3):
        res = {'query_id': query_id,
               'action_proba': proba_action[i][0] * 100,
               'action_id': proba_action[i][1],
               'action_text': proba_action[i][2],
               'view_order': i}
        result = Result(**res)
        results.append(res)
        db.session.add(result)
    db.session.commit()
    return results


def create_response(query_id, result_num):
    """
    When user selected an action from the results,
    creates :class:`text2bank_app.models.Response` and adds it to the DB.

    Parameters
    ----------
    query_id
    result_num: int
        view/predicted order of the result of query

    """
    current_app.logger.debug(f'got response: {result_num} '
                             f'on query_id: {query_id}')

    result = db.session.query(Result).filter(
        and_(Result.query_id == query_id, Result.view_order == result_num)
    ).first()

    current_app.logger.debug(f'got result: {result}')

    response = Response(
        query_id=query_id,
        result_id=result.id
    )
    db.session.add(response)
    current_app.logger.debug(f'response no. {result_num} added. '
                             f'query_id: {query_id}, result_id: {result.id}')
    db.session.commit()


def create_else_response(chosen_action, query_id, user_explanation):
    """
    When user did not selected anything from the results,
    but wants to leave a comment or select any other action,
    creates "something else" :class:`text2bank_app.models.Response`
    and adds it to the DB.

    Parameters
    ----------
    chosen_action
        from dropdown menu
    query_id
    user_explanation: str
       user's explanation
    """
    current_app.logger.debug('got response: something else')
    response = Response(
        query_id=query_id,
        user_explanation=user_explanation,
        chosen_action=chosen_action,
    )
    db.session.add(response)

    current_app.logger.debug(
        f'response: something-else added, '
        f'query_id: {query_id}, user_explanation: {user_explanation}, '
        f'chosen_action: {chosen_action}')
    db.session.commit()


def full_queries_responses():
    """
    Lists all Queries with User information.
    Joins Result and Response to show the interaction.

    Returns
    -------
    pandas.DataFrame
        columns: username, user_email, (query) input, action_selected,
        viewed_order, action_other (dropdown menu), user_explanation
    """
    result = db.session.query(
        User.name, Query.input,
        Response.result_id, Response.chosen_action, Response.user_explanation
    ).join(Query, Query.user_id == User.id).join(Query.response)

    data = []
    for r in result:
        d = {
            'name': r.name,
            'input': r.input,
            'action_selected': None,
            'viewed_order': None,
            'action_other (dropdown menu)': r.chosen_action,
            'user_explanation': r.user_explanation,
        }
        q_r = db.session.query(
            Result.view_order, Result.action_text
        ).filter(Result.id == r.result_id).first()
        if q_r:
            d.update({'viewed_order': q_r.view_order,
                      'action_selected': q_r.action_text})
        data.append(d)
    df = pd.DataFrame(data)
    df['viewed_order'] = df['viewed_order'].astype(float)
    return df


def accuracy_report(queries_responses_df):
    """
    Aggregates users' responses into an accuracy report,
    showing top_1 and top_3 accuracy.

    Parameters
    ----------
    queries_responses_df: pandas.DataFrame
        columns: viewed_order, action_selected, action_other (dropdown menu)
    Returns
    -------

    """

    print(queries_responses_df.columns)
    df = queries_responses_df
    df['viewed_order'] = df['viewed_order'].fillna(np.inf)
    sel = df['action_selected'].fillna('')
    oth = df['action_other (dropdown menu)'].fillna('')
    df['action'] = (sel + oth).astype(str)
    df['top_1'] = df['viewed_order'] == 0
    df['top_3'] = df['viewed_order'] < 4
    df['top_1'] = df['top_1'].astype(float)
    df['top_3'] = df['top_3'].astype(float)

    top_n = df.groupby('action').mean().drop(columns=['viewed_order'])
    count = df.groupby('action').count()['top_1'].rename('n_queries')
    report = pd.concat([top_n, count], axis=1)
    report = report.reset_index()
    report.loc[(report['action'] == ''), 'action'] = 'NO ACTION CHOSEN'

    return report


def insert_dummy_responses_anonym():
    """
    Fill the database with some dummy users, queries and responses

    """
    if not db.session.query(User).first():
        current_app.logger.info(
            'Inserting DEV user: dev')
        user_d = User(name="dev")
        db.session.add(user_d)

        user_p = User(name="peter")
        db.session.add(user_p)

    if not db.session.query(Query).first():
        user_id = db.session.query(User).filter(User.name == 'dev').first().id
        _dummy_reponse('poslat prachy', user_id, 0)
        _dummy_reponse('zrušit inkaso', user_id, 0)
        _dummy_reponse('výpis', user_id, 0)
        _dummy_reponse('jaké mám čekají platby?', user_id, 0)
        _dummy_reponse('Jaké platby mám zadané k úhradě?', user_id, 1)
        _dummy_reponse('blokováno', user_id, 2)
        _dummy_reponse('historie', user_id, None, 'Výpisy', None)
        _dummy_reponse('limit na kartě?', user_id, None, None, 'Limity')
        _dummy_reponse('přijaté platby leden', user_id, None, 'Výpisy')
        user_id = db.session.query(User).filter(User.name == 'peter').first().id
        _dummy_reponse('poslat 500 matce', user_id, 0)
        _dummy_reponse('1+1', user_id, None, None, 'blbost')


def insert_dummy_responses():
    """
    Fill the database with some dummy users, queries and responses

    """
    if not db.session.query(User).first():
        current_app.logger.info(
            'Inserting DEV user: email: dev@dev.com, password: development')
        user_d = User(name="Dev", email="dev@dev.com")
        user_d.set_password("develop")
        db.session.add(user_d)

        user_p = User(name="peter", email="peter@dev.com")
        user_p.set_password("develop")
        db.session.add(user_p)

    if not db.session.query(Query).first():
        user_id = db.session.query(User).filter(User.name == 'Dev').first().id
        _dummy_reponse('poslat prachy', user_id, 0)
        _dummy_reponse('zrušit inkaso', user_id, 0)
        _dummy_reponse('výpis', user_id, 0)
        _dummy_reponse('jaké mám čekají platby?', user_id, 0)
        _dummy_reponse('Jaké platby mám zadané k úhradě?', user_id, 1)
        _dummy_reponse('blokováno', user_id, 2)
        _dummy_reponse('historie', user_id, None, 'Výpisy', None)
        _dummy_reponse('limit na kartě?', user_id, None, None, 'Limity')
        _dummy_reponse('přijaté platby leden', user_id, None, 'Výpisy')
        user_id = db.session.query(User).filter(User.name == 'peter').first().id
        _dummy_reponse('poslat 500 matce', user_id, 0)
        _dummy_reponse('1+1', user_id, None, None, 'blbost')


def _dummy_reponse(query_input, user_id, result_num,
                   chosen_action=None, user_explanation=None):
    query = add_query(query_input, user_id)

    evaluator = current_app.config['evaluator']
    evaluator.fit(query_input)

    results = evaluator.evaluate(sort_output=True)
    results_json = write_results(query.id, results)

    current_app.logger.debug(f'results written: {results_json}')
    if result_num is not None:
        create_response(query.id, result_num)
    else:
        create_else_response(chosen_action, query.id, user_explanation)
