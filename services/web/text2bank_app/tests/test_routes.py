import os

import flask

cur_dir = os.path.dirname(__file__)
os.chdir(os.path.join(cur_dir, '..', '..'))


def test_app(_app):
    assert isinstance(_app, flask.Flask)


def test_login(_app_client):
    response = _app_client.get('/login')
    assert response.status_code == 200
    assert 'Přihlaš se se svými údaji' in response.get_data(as_text=True)
    assert 'Přihlášení.' in response.get_data(as_text=True)
    assert 'Nemáte účet?' in response.get_data(as_text=True)
    assert 'Registrujte se' in response.get_data(as_text=True)


def test_signup(_app_client):
    response = _app_client.get('/signup')
    assert response.status_code == 200
    assert 'Vytvořit účet.' in response.get_data(as_text=True)
    assert 'Registruj se pro přihlášení.' in response.get_data(as_text=True)


def test_dashboard(_app, test_user, _app_client):
    response = _app_client.get('/')
    assert response.status_code == 200
    assert 'Jste přihlášen/a!' in response.get_data(as_text=True)
