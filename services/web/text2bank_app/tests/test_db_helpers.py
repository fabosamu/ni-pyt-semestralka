import pytest
from numpy.testing import assert_array_equal

from text2bank_app import db_helpers as dbh
from text2bank_app import models as mdl


def test_db(_db):
    dbh.insert_dummy_responses()

    query_res = _db.session.query(mdl.User).all()
    assert 'Dev' in [u.name for u in query_res]


@pytest.mark.parametrize(['name', 'mail', 'password'], [
    ('Test', 'test@test.com', 'testing'),
    ('Test1', 'test1@test.com', 'tst')
])
def test_signup_user(_db, name, mail, password):
    user_ret = dbh.signup_user(name, mail, password)
    user_act = _db.session.query(mdl.User).first()

    assert isinstance(user_ret, mdl.User)

    assert user_act.name == name
    assert user_act.name == user_ret.name
    assert user_act.email == mail
    assert user_act.email == user_ret.email
    assert user_act.password != password
    assert user_ret.password != password


def test_add_query(_db):
    user_ret = dbh.signup_user('Test', 'test@test.com', 'testing')
    query = dbh.add_query('some query', user_ret.id)
    query_act = _db.session.query(mdl.Query).first()

    assert isinstance(query, mdl.Query)

    assert query.input == 'some query'
    assert query_act.input == 'some query'
    assert query.user_id == user_ret.id
    assert query_act.user_id == user_ret.id
    assert query.results == []
    assert query_act.results == []
    assert query.response == []
    assert query_act.response == []


def test_write_results(_db):
    user_ret = dbh.signup_user('Test', 'test@test.com', 'testing')
    query = dbh.add_query('some query', user_ret.id)

    proba_actions = [(0.4, 10, 'action1'), (0.3, 20, 'action2'),
                     (0.2, 30, 'action3'), (0.1, 40, 'action4')]

    result_dicts = dbh.write_results(query.id, proba_actions)

    results = _db.session.query(mdl.Result).all()
    assert len(results) == 3

    results = sorted(results, key=lambda res: res.view_order, reverse=False)

    for res, p_act, res_d in zip(results, proba_actions, result_dicts):
        assert isinstance(res, mdl.Result)
        assert res.action_proba == p_act[0] * 100 == res_d['action_proba']
        assert res.action_id == p_act[1] == res_d['action_id']
        assert res.action_text == p_act[2] == res_d['action_text']
        assert res.query_id == query.id


def test_create_response(_db, user_query_result):
    query = user_query_result
    view_order = 1
    result = _db.session.query(mdl.Result).filter_by(
        view_order=view_order).first()

    dbh.create_response(query.id, view_order)

    response_act = _db.session.query(mdl.Response).all()

    assert len(response_act) == 1
    response_act = response_act[0]

    assert response_act.result_id == result.id
    assert response_act.query_id == query.id
    assert response_act.user_explanation is None
    assert response_act.chosen_action is None


def test_create_else_response(_db, user_query_result):
    query = user_query_result
    view_order = 1
    _ = _db.session.query(mdl.Result).filter_by(
        view_order=view_order).first()

    dbh.create_else_response('other action', query.id, 'some explanation')

    response_act = _db.session.query(mdl.Response).all()

    assert len(response_act) == 1
    response_act = response_act[0]

    assert response_act.result_id is None
    assert response_act.query_id == query.id
    assert response_act.user_explanation == 'some explanation'
    assert response_act.chosen_action == 'other action'


def test_full_queries_responses(_db):
    dbh.insert_dummy_responses()
    df = dbh.full_queries_responses()
    assert_array_equal(df.columns, [
        'username', 'user_email', 'input', 'action_selected', 'viewed_order',
        'action_other (dropdown menu)', 'user_explanation'])
    assert len(df) == 11


def test_accuracy_report(_db):
    dbh.insert_dummy_responses()
    df = dbh.full_queries_responses()
    report = dbh.accuracy_report(df)

    print(report)

    assert len(report) == 5
    assert report['n_queries'].sum() == len(_db.session.query(mdl.Query).all())
    assert_array_equal(report.columns, [
        'action', 'top_1', 'top_3', 'n_queries'])
