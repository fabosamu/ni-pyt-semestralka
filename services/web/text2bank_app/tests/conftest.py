import os

import pytest
from dotenv import load_dotenv

import text2bank_app.db_helpers as dbh
from text2bank_app import db, create_app

cur_dir = os.path.dirname(__file__)
os.chdir(os.path.join(cur_dir, '..', '..'))
load_dotenv(dotenv_path=os.path.join(cur_dir, '.env'))


@pytest.fixture
def flask_config():
    class Config(object):
        """Set Flask configuration from environment variables."""

        FLASK_APP = 'wsgi.py'
        FLASK_ENV = os.getenv('FLASK_ENV')
        SECRET_KEY = os.getenv('SECRET_KEY')
        RECIPE_PATH = os.getenv('RECIPE_PATH')

        # Static Assets
        STATIC_FOLDER = 'static'
        TEMPLATES_FOLDER = 'templates'

        # Flask-SQLAlchemy
        SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "sqlite://")
        SQLALCHEMY_TRACK_MODIFICATIONS = False

        # Testing
        TESTING = True
        # WTF_CSRF_ENABLED = False
        LOGIN_DISABLED = True

    return Config


@pytest.fixture
def _app(flask_config):
    app = create_app(flask_config)
    app.app_context().push()
    return app


@pytest.fixture
def _app_client(_app, test_user):
    cl = _app.test_client()
    return cl


@pytest.fixture
def _db(_app):
    db.drop_all()
    db.create_all()
    return db


@pytest.fixture
def test_user(_db):
    return dbh.signup_user('Test', 'test@test.com', 'testing')


@pytest.fixture
def user_query_result(_db):
    user_ret = dbh.signup_user('Test', 'test@test.com', 'testing')
    query = dbh.add_query('some query', user_ret.id)
    proba_actions = [(0.4, 10, 'action1'), (0.3, 20, 'action2'),
                     (0.2, 30, 'action3'), (0.1, 40, 'action4')]
    _ = dbh.write_results(query.id, proba_actions)
    return query
