USER_EXISTS = 'Uživatel s takovým emailem již existuje.'
ACCOUNT_CREATE = 'Vytvořit účet.'
REGISTER = 'Registruj se pro přihlášení.'
USER_NOT_VALID = 'Kombinace emailu a hesla není validní'
LOGIN = 'Přihlášení'
LOGIN_TEXT = ''
REGISTER_BEFORE_LOGIN = 'Prosím, zadej své jméno nebo E-mail'

TEXT2BANK = 'Text2Bank aplikace'
QUERY_RESULTS = 'Výsledky dotazu'

FORM_DOTAZ = 'Co mám udělat?'
FORM_RESP_ELSE = 'Něco jiného'
FORM_RESP_NONE_ABOVE = 'Žádný z výše uvedených'
FORM_RESP_SUBMIT_ELSE = 'Odeslat volbu a komentář'
FORM_QUERY_SUBMIT = 'Odeslat'
 