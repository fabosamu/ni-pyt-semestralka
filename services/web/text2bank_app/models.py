from flask_login import UserMixin
from sqlalchemy.sql import func
from werkzeug.security import generate_password_hash, check_password_hash

from .text2bank import db


class User(UserMixin, db.Model):
    """Anonym User"""
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = name = db.Column(db.String(100), unique=False, nullable=True)
    created_on = db.Column(db.DateTime, index=False, unique=False,
                           nullable=True, server_default=func.now())
    
    queries = db.relationship('Query', backref='query', lazy=True)

    def __repr__(self):
        return f'<User: name: {self.name}, created_on: {self.created_on}>'



# class User(UserMixin, db.Model):
#     """User account model."""

#     __tablename__ = 'user'
#     id = db.Column(db.Integer, primary_key=True)
#     email = db.Column(db.String(40), unique=True, nullable=False)
#     password = db.Column(
#         db.String(200), primary_key=False, unique=False, nullable=False)
#     name = db.Column(db.String(100), unique=False, nullable=False)
#     created_on = db.Column(db.DateTime, index=False, unique=False,
#                            nullable=True, server_default=func.now())
#     last_login = db.Column(db.DateTime, index=False, unique=False,
#                            nullable=True, onupdate=func.now())

#     queries = db.relationship('Query', backref='query', lazy=True)

#     def set_password(self, password):
#         # Create hashed password.
#         self.password = generate_password_hash(password, method='sha256')

#     def check_password(self, password):
#         # Check hashed password
#         return check_password_hash(self.password, password)

#     def __repr__(self):
#         return f'<User: name: {self.name}>'


class Query(db.Model):
    """Query holding input text and user who made it."""

    __tablename__ = 'query'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    # user_id = db.Column(db.Integer, db.ForeignKey('useranonym.id'), nullable=False)

    input = db.Column(db.Text, nullable=False, unique=False)
    created_on = db.Column(db.DateTime, index=False, unique=False,
                           nullable=True, server_default=func.now())

    results = db.relationship('Result', backref='result', lazy=False)
    response = db.relationship('Response', backref='response', lazy=True)

    def __repr__(self) -> str:
        return f'<Query: input: {self.input}, ' \
               f'user_id: {self.user_id}>'


class Result(db.Model):
    """Result of the query - one of predicted and displayed actions."""

    __tablename__ = 'result'
    id = db.Column(db.Integer, primary_key=True)
    query_id = db.Column(db.Integer, db.ForeignKey('query.id'), nullable=False)

    action_id = db.Column(db.Integer, nullable=False, unique=False)
    action_text = db.Column(db.Text, nullable=False, unique=False)
    action_proba = db.Column(db.Integer, nullable=False, unique=False)
    view_order = db.Column(db.Integer, nullable=False, unique=False)

    def __repr__(self) -> str:
        return f'<Result: action_text: {self.action_text}, ' \
               f'action_id: {self.action_id}, ' \
               f'action_prob: {self.action_proba}, ' \
               f'view_order: {self.view_order}, ' \
               f'query_id: {self.query_id}>'


class Response(db.Model):
    """User's response for given actions.
    Either he/she chooses one or one from dropdown menu
    or provides some explanation."""

    __tablename__ = 'response'
    id = db.Column(db.Integer, primary_key=True)
    result_id = db.Column(db.Integer, db.ForeignKey('result.id'), nullable=True)
    responded_on = db.Column(db.DateTime, index=False, unique=False,
                             nullable=True, server_default=func.now())

    query_id = db.Column(db.Integer, db.ForeignKey('query.id'), nullable=False)
    query = db.relationship("Query", uselist=False, back_populates="response")

    user_explanation = db.Column(db.Text, unique=False, nullable=True)
    chosen_action = db.Column(db.Text, unique=False, nullable=True)

    def __repr__(self) -> str:
        return f'<Response: result_id: {self.result_id}, ' \
               f'chosen_action: {self.chosen_action} ' \
               f'user_expl.: {self.user_explanation}>'


class Version(db.Model):
    """Current version of the app."""

    __tablename__ = 'version'
    id = db.Column(db.Integer, primary_key=True)

    used_from = db.Column(db.DateTime, index=True, unique=True,
                          nullable=True, server_default=func.now())
    commit_hash = db.Column(db.Text, nullable=False, unique=True)
    version_number = db.Column(db.Text, nullable=False, unique=True)

    recipe_text = db.Column(db.Text, nullable=True, unique=False)
    description = db.Column(db.Text, nullable=True, unique=False)

    def __repr__(self) -> str:
        return f'<Version: {self.version_number}>'
