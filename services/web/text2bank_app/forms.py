from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from wtforms.validators import DataRequired, Email, EqualTo, Length
from text2bank_app import text as txt


class QueryForm(FlaskForm):
    """Form for getting user's query as an input."""

    input = StringField(txt.FORM_DOTAZ,
                        validators=[DataRequired(), 
                        Length(min=3, max=420, message='Dotaz od 3 do 420 písmen')])
    submit = SubmitField(txt.FORM_QUERY_SUBMIT)


class QueryResponseForm(FlaskForm):
    """User response form with three best classified actions as results,
    dropdown menu with all actions and textfield for description"""

    res0 = SubmitField('Výsledek 1')
    res1 = SubmitField('Výsledek 2')
    res2 = SubmitField('Výsledek 3')

    actions = SelectField(txt.FORM_RESP_ELSE, coerce=str, validate_choice=False)
    input_else = StringField(txt.FORM_RESP_NONE_ABOVE)

    res_else = SubmitField(txt.FORM_RESP_SUBMIT_ELSE)


class SignupForm(FlaskForm):
    """User Sign-up Form."""

    name = StringField(
        'Jméno',
        validators=[DataRequired()]
    )
    email = StringField(
        'Email',
        validators=[
            Length(min=6),
            Email(message='Zadej validní email.'),
            DataRequired()
        ]
    )
    password = PasswordField(
        'Heslo',
        validators=[
            DataRequired(),
            Length(min=6, message='Zvol, prosím, dlhší heslo.')
        ]
    )
    confirm = PasswordField(
        'Potvrďte své heslo',
        validators=[
            DataRequired(),
            EqualTo('password', message='Heslá se musí zhodovat.')
        ]
    )
    submit = SubmitField('Registruj')


class LoginForm(FlaskForm):
    """User Log-in Form."""

    email = StringField(
        'Email',
        validators=[
            DataRequired(),
            Email(message='Zadej validní email.')
        ]
    )
    password = PasswordField('Heslo', validators=[DataRequired()])
    submit = SubmitField('Přihlásit se')


class LoginFormAnonym(FlaskForm):
    name = StringField(
        'Jméno / E-mail',
        validators=[DataRequired(), 
        Length(min=3, max=20, message='Prosím zadej jmnéno / email od 3 do 20 písmen')],
        )
    submit = SubmitField('Přihlásit se')

