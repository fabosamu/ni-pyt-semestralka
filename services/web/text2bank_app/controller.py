from flask import session, current_app, make_response, url_for
from flask_login import current_user
from werkzeug.utils import redirect

from text2bank_app import db_helpers as dbh
from text2bank_app.forms import QueryResponseForm

from transformer_eval.celery_workers import get_kb_max_simil, predict


# def evaluate_submitted_query_nb(query_form):
#     session['query_input'] = query_form.input.data
#     query = dbh.add_query(session['query_input'], current_user.id)

#     entities, results, not_sure = evaluating_pipeline(
#         query_input=session['query_input'],
#         evaluator=current_app.config['evaluator'])

#     current_app.logger.debug(f'Extracted entities: {entities}')

#     result_dicts = dbh.write_results(query.id, results)
#     write_to_session(entities, query, result_dicts, not_sure)

#     return make_response(redirect(url_for('main_bp.results')))


# def evaluate_submitted_query(query_form):
#     session['query_input'] = query_form.input.data
#     query = dbh.add_query(session['query_input'], current_user.id)

#     entities, _, _ = evaluating_pipeline(
#         query_input=session['query_input'],
#         evaluator=current_app.config['evaluator'])

#     current_app.logger.debug(f'Extracted entities: {entities}')

#     results, not_sure = transformer_results(
#         query_input=session['query_input'],
#         evaluator=current_app.config['transformer_eval'],
#         validator=current_app.config['clf_ood'])

#     result_dicts = dbh.write_results(query.id, results)

#     current_app.logger.debug(f'Predicted dict results: {result_dicts}')

#     write_to_session(entities, query, result_dicts, not_sure)

#     return make_response(redirect(url_for('main_bp.results')))


# def transformer_results(query_input, evaluator, validator):
#     # predictions = evaluator.predict(query_input, sort_results=True)
#     # not_sure = validator.get_kb_similarity([query_input])[0]['max_simil'] < 0.7
#     return predictions, not_sure


def evaluate_submitted_query(query_form):
    session['query_input'] = query_form.input.data
    query = dbh.add_query(session['query_input'], current_user.id)

    entities, _, _ = evaluating_pipeline(
        query_input=session['query_input'],
        evaluator=current_app.config['evaluator'])

    current_app.logger.debug(f'Extracted entities: {entities}')

    # results, not_sure = transformer_results(
    #     query_input=session['query_input'],
    #     evaluator=current_app.config['transformer_eval'],
    #     validator=current_app.config['clf_ood'])

    result = get_kb_max_simil.apply_async((session['query_input'],), expires=60)
    max_simil = result.get()
    # results = d['predictions']
    # max_simil = d['not_sure']

    evaluator = current_app.config['transformer_eval']
    results = evaluator.predict(session['query_input'], sort_results=True)
    # result = predict.apply_async((session['query_input'],), expires=60)
    # results = result.get()
    
    not_sure = bool(max_simil < 0.7)

    result_dicts = dbh.write_results(query.id, results)

    current_app.logger.debug(f'Predicted dict results: {result_dicts}')

    write_to_session(entities, query, result_dicts, not_sure)

    return make_response(redirect(url_for('main_bp.results')))



def evaluating_pipeline(query_input, evaluator):
    evaluator.fit(query_input)
    current_app.logger.debug(f'Evaluator fitted. Tokens: {evaluator.tokens}')
    results = evaluator.evaluate(sort_output=True)
    entities = evaluator.entities
    current_app.logger.debug(f'Evaluator evaluated probabilities for actions. '
                             f'Tokens: {evaluator.tokens}, '
                             f'Entities: {entities}, Results: {results}')
    probs = [r[0] for r in results]
    not_sure = bool(min(probs) == max(probs))
    return entities, results, not_sure


def write_to_session(entities, query, result_dicts, not_sure):
    session['query_id'] = query.id
    session['query_results'] = result_dicts
    session['entities'] = entities
    session['not_sure'] = not_sure


def validate_and_create_response(query_response):
    current_app.logger.debug('validating on submit')
    if query_response.res0.data:
        dbh.create_response(session['query_id'], result_num=0)
    if query_response.res1.data:
        dbh.create_response(session['query_id'], result_num=1)
    if query_response.res2.data:
        dbh.create_response(session['query_id'], result_num=2)
    if query_response.res_else.data:
        dbh.create_else_response(query_response.actions.data,
                                 session['query_id'],
                                 query_response.input_else.data)


def modified_query_response_form():
    query_resp = QueryResponseForm()

    # action_names = current_app.config['evaluator'].action_names
    action_names = sorted(current_app.config['transformer_eval'].action_names, reverse=False)
    choices = [('', 'Zvol jeden z ostatních záměrů')] + [(g, g)
                                                         for g in action_names]
    query_resp.actions.choices = choices

    query_results = session['query_results']
    action_probas = [qr['action_proba'] for qr in query_results[:3]]
    action_texts = [qr['action_text'] for qr in query_results[:3]]

    query_resp.res0.label.text = f'{action_probas[0]:.2f}%: {action_texts[0]}'
    query_resp.res1.label.text = f'{action_probas[1]:.2f}%: {action_texts[1]}'
    query_resp.res2.label.text = f'{action_probas[2]:.2f}%: {action_texts[2]}'

    return query_resp


def clean_cookies():
    current_app.logger.debug('response commited')
    session.pop('query_id', None)
    session.pop('query_results', None)
    session.pop('entities', None)
    session.pop('not_sure', None)
