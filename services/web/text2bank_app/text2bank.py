import yaml
from pathlib import Path
from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

import nb_query_eval.helpers as hlp
from nb_query_eval.ensemble import Ensemble
from nb_query_eval import Tokenizer
from nb_query_eval.entity_recognizer import EntityRecognizer
import nb_query_eval.query_evaluator as qe

import pandas as pd

from transformer_eval import Evaluator
# from transformer_eval import  ClassifierOOD

db = SQLAlchemy()
login_manager = LoginManager()


cat2label = {'aktivovat moji kartu': 0, 'bezkontaktní platba nefunguje': 1, 'karta nefunguje': 2,
             'karta spolknuta': 3, 'zablokovaný pin': 4, 'karta zneužita': 5, 'limity jednorázových karet': 6,
             'délka doby převodu ': 7, 'nesprávná částka přijaté hotovosti z bankomatu': 8, 'neúspěšný převod': 9,
             'odhad času doručení karty': 10, 'odmítnuta platba kartou': 11, 'odmítnutý převod': 12,
             'odmítnutý výběr hotovosti': 13, 'ověřit zdroj financí': 14, 'platba inkasem nebyla rozpoznána': 15,
             'nerozpoznávám platbu kartou': 16, 'platba kartou špatný směnný kurz': 17, 'platnost karty brzy skončí': 18,
             'podpora bankomatu': 19, 'podpora fiat měny': 20, 'podpora země': 21, 'poplatek za platbu kartou': 22,
             'poplatek za výběr hotovosti': 23, 'požadovat vrácení peněz': 24, 'příjemce neobdržel převod': 25,
             'přijímání karet': 26, 'příchod karty': 27, 'příjemce převodu není povolen': 28, 'poplatek navíc ve výpisu': 29,
             'směnný kurz': 30, 'transakce účtována dvakrát': 31, 'ukončit účet': 32, 'upravit osobní údaje': 33,
             'virtuální karta nefunguje': 34, 'vrácení peněz se nezobrazuje': 35, 'vrácena platba kartou?': 36, 'změnit pin': 37,
             'visa nebo mastercard': 38, 'věkový limit': 39, 'zapomenuté přístupové heslo nebo kód': 40, 'zrušit převod': 41,
             'ztracená nebo odcizená karta': 42, 'ztracený nebo odcizený telefon': 43, 'získat jednorázovou virtuální kartu': 44,
             'získání náhradní karty': 45, 'získání virtuální karty': 46, 'zůstatek není aktualizován po bankovním převodu': 47,
             'zůstatek není aktualizován po šeku nebo vkladu hotovosti': 48, 'účtovaný poplatek za převod': 49, 'čekající platba kartou': 50,
             'čekající převod': 51, 'čekající výběr hotovosti': 52, 'špatný směnný kurz pro výběr hotovosti': 53, 'chci půjčit/kolik mi půjčíte': 54,
             'jednorázová platba tuzemská': 55, 'potřebuji mimořádnou splátku (hypotéky)': 56, 'potřebuji potvrzení o platbě': 57, 'sjednat hypotéku': 58,
             'sjednat schůzku na pobočce': 59, 'splátka kreditní karty': 60, 'svolení k inkasu a sipo': 61, 'trvalé platby': 62, 'výpisy': 63}



def create_app(config_obj=None):
    """Constructs the core app object."""
    app = Flask(__name__, instance_relative_config=False)

    # Application Configuration
    if config_obj:
        app.config.from_object(config_obj)
    else:
        app.config.from_object('config.Config')

    # Initialize Plugins
    db.init_app(app)
    login_manager.init_app(app)

    with app.app_context():
        from . import auth
        from . import routes

        # Register Blueprints
        app.register_blueprint(routes.main_bp)
        app.register_blueprint(auth.auth_bp)

        recipe, recipe_text = read_yaml(app.config['RECIPE_PATH'])
        evaluator = create_ensemble(recipe)
        app.config['evaluator'] = evaluator

        app.config['action_names'] = cat2label.keys()

        tfmr_eval = Evaluator(Path(recipe['transformer_path']))
        app.config['transformer_eval'] = tfmr_eval

        # df_merged = pd.read_csv('data_text2bank/merged-2022-03-24.csv')
        # kb = pd.read_csv(Path(recipe['knowledge_base_path']))

        # p = Path('/fastText')
        # app.logger.info([x for x in p.iterdir() if x.is_dir()])
        # p = Path('/')
        # app.logger.info([x for x in p.iterdir() if x.is_dir()])

        app.config['tooltips'] = pd.read_csv(recipe['tooltips_path'])['text_cz']

        return app


def create_ensemble(recipe):
    """
    Create pretrained ensemble model for classification.
    Now two models/evaluators for ensemble:
    :class:`nb_query_eval.entity_recognizer.EntityRecognizer` and
    :class:`nb_query_eval.query_evaluator.QueryEvaluator`

    Parameters
    ----------
    recipe: YamlConfiguration

    Returns
    -------
    :class:`nb_query_eval.ensemble.Ensemble`
        evaluator

    """
    try:
        kw_mat = hlp.read_keyword_matrix(recipe['keyword_matrix_path'])
        currencies = hlp.read_currencies(recipe['currencies_path'])
        stop_words = hlp.read_stop_words(recipe['stop_words_path'])
        entity_matrix = hlp.read_entity_matrix(recipe['entity_matrix_path'])
    except KeyError as ke:
        raise RuntimeError('Some path in recipe is missing. ', ke)

    evaluators = [
        EntityRecognizer(
            entity_matrix,
            currencies,
            prior_denominator=recipe['nb_parameters']['prior_denominator'],
            general_bf=recipe['nb_parameters']['general_bf'],
        ),
        qe.QueryEvaluator(
            keyword_matrix=kw_mat,
            **recipe['nb_parameters']
        ),
        qe.QueryEvaluatorNoAccents(
            keyword_matrix=kw_mat,
            **recipe['nb_parameters']
        ),
    ]
    tokenizer = Tokenizer(stop_words)
    action_names = kw_mat.columns
    evaluator = Ensemble(action_names, evaluators, tokenizer,
                         weights=recipe['weights'])
    return evaluator


def read_yaml(yaml_path):
    """
    Reads yaml file from given path.

    Parameters
    ----------
    yaml_path: str

    Returns
    -------
    YamlConfiguration
        recipe
    """
    with open(yaml_path, 'r') as stream:
        try:
            return yaml.safe_load(stream), stream.read()
        except yaml.YAMLError as exc:
            print('error while reading yaml!')
            raise exc
    raise RuntimeError('Cannot load yaml recipe')
