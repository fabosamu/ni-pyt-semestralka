from flask import Blueprint, render_template, redirect, \
    url_for, make_response, session, current_app
from flask_login import current_user, login_required

from text2bank_app import db_helpers as dbh
from text2bank_app.controller import evaluate_submitted_query, \
    validate_and_create_response, modified_query_response_form, clean_cookies
from text2bank_app.forms import QueryForm
from text2bank_app import text as txt

main_bp = Blueprint(
    'main_bp', __name__,
    template_folder='templates',
    static_folder='static'
)


@main_bp.route('/', methods=['GET', 'POST'])
@login_required
def dashboard():
    """Dashboard with a query form for user input."""
    query_form = QueryForm()

    if query_form.validate_on_submit():
        return evaluate_submitted_query(query_form)

    return render_template(
        'dashboard.html',
        title=txt.TEXT2BANK,
        query_form=query_form,
        template='dashboard-template',
        current_user=current_user,
    )

@main_bp.route('/tip', methods=['GET', 'POST'])
@login_required
def dashboard_tip():
    """Dashboard with a query form for user input."""
    query_form = QueryForm()

    if query_form.validate_on_submit():
        return evaluate_submitted_query(query_form)

    return render_template(
        'dashboard_tip.html',
        title=txt.TEXT2BANK,
        query_form=query_form,
        template='dashboard-template',
        current_user=current_user,
        query_tip=current_app.config['tooltips'].sample(1).iloc[0]
    )


@main_bp.route('/results', methods=['GET', 'POST'])
@login_required
def results():
    """Results page with 3 actions with highest probability,
    textfield and dropdown menu when evaluated badly"""

    query_response = modified_query_response_form()

    if query_response.validate_on_submit():
        validate_and_create_response(query_response)

        clean_cookies()
        return make_response(redirect(url_for('main_bp.thanks')))

    return render_template(
        'results.html',
        title=txt.QUERY_RESULTS,
        query_response=query_response,
        template='dashboard-template',
        current_user=current_user,
        entities=session['entities'],
        query_text=session['query_input'],
        not_sure=session['not_sure'],
    )

@main_bp.route('/thanks', methods=['GET'])
@login_required
def thanks():
    return render_template(
            'thanks.html', 
            current_user=current_user,
            template='help-template'
        )

@main_bp.route("/report")
@login_required
def report():
    """Prints out accuracy report for each class.
    showing top_1 and top_3 accuracy of the model and
    how many times any action was chosen (count).
    """
    responses = dbh.full_queries_responses()
    results = dbh.accuracy_report(responses)
    return results.to_html() 


@main_bp.route("/help")
def help():
    """Shows help"""
    return render_template(
        'help.html',
        title=txt.TEXT2BANK,
        template='help-template',
    )

