import re

import nltk


class Tokenizer:
    """
    Helper class wrapping nltk to unify tokenizing,
    if any other tokenizer in use.
    """

    def __init__(self, stop_words: list) -> None:
        """
        Set list of stop words depending on the specified language of deployment

        Parameters
        ----------
        stop_words: list of str
        """
        self.r_thousands = re.compile(r'(\d+\.?\d*)k\s')
        self.r_exponent = re.compile(r'(\d+\.?\d*)e(\d+)')
        self.r_amt_notation = re.compile(r',-')
        self.stop_words = stop_words
        self.r_num = re.compile(r'\d+')
        self.r_nkw = re.compile(r'\W', re.UNICODE)
        self.r_decimal = re.compile(r'(\d+)[,](\d+)')
        self.r_num_pairs = re.compile(r'(\d+)[ ´`\'](\d+)')

    def fit(self, query_text: str):
        """
        Fits the query text and returns extracted list of tokens as:

        * KW: possible keyword
        * NUM: only a number
        * NKW: not a keyword, contains non-letters
        * SW: stopword from list of stopwords

        Parameters
        ----------
        query_text: str
            query text from input
        Returns
        -------
        list of tuples
            tokens as list of (word string, token_classification string)
        """
        query_text = query_text + ' '
        query_text = self.preprocess(query_text.lower())
        split = nltk.word_tokenize(query_text)
        tokens = []
        for w in split:
            if re.search(self.r_num, w):
                tokens.append((w, 'NUM'))
            elif re.search(self.r_nkw, w):
                tokens.append((w, 'NKW'))
            elif w.lower() in self.stop_words:
                tokens.append((w, 'SW'))
            else:
                tokens.append((w, 'KW'))
        return tokens

    def preprocess(self, query_text):
        query_text = re.sub(self.r_amt_notation, 'czk', query_text)

        # replace decimal separators

        query_text = re.sub(self.r_decimal, r'\1.\2', query_text)

        query_text = self.convert_thousands(query_text)
        query_text = self.convert_milions(query_text)

        query_text = self.convert_numbers_with_exponent(query_text)

        # find separated pair of numbers
        query_text = self.remove_thousand_separators(query_text,
                                                     self.r_num_pairs)

        return query_text

    def remove_thousand_separators(self, query_text, r):
        while re.search(r, query_text) is not None:
            # remove separator
            query_text = re.sub(r, r'\1\2', query_text)
        return query_text

    def convert_numbers_with_exponent(self, query_text):
        m = re.search(self.r_exponent, query_text)
        if m:
            num = m.group(1) + 'e' + m.group(2)
            try:
                converted_num = float(num)
                query_text = re.sub(self.r_exponent, str(converted_num),
                                    query_text)
            except ValueError:
                pass
        return query_text

    def convert_thousands(self, query_text):
        # prerequisite: decimal separator converted
        # look for e.g., 100k
        query_text = self.convert_times_char(query_text, self.r_thousands, 1000)
        # same as above, but can finishes with EOL
        # TODO: remove endline guys, maybe not needed if adding space in the end
        r = re.compile(r'(\d+\.?\d*)k$')
        query_text = self.convert_times_char(query_text, r, 1000)
        return query_text

    def convert_times_char(self, query_text, r, times):
        # with given precompiled regex, search for occurences and multiply.
        m = re.search(r, query_text)
        if not m:
            return query_text
        num = float(m.group(1))
        num = int(num * times)
        return re.sub(r, str(num) + ' ', query_text)

    def convert_milions(self, query_text):
        # prerequisite: decimal separator converted
        # look for e.g., 1m
        r = re.compile(r'(\d+\.?\d*)\s?m\s')
        query_text = self.convert_times_char(query_text, r, 1000000)
        # same as above, but can finish with EOL
        r = re.compile(r'(\d+\.?\d*)\s?m$')
        query_text = self.convert_times_char(query_text, r, 1000000)
        # look for e.g., 1mil*
        r = re.compile(r'(\d+\.?\d*)\s?mil\w*[\s.]')
        query_text = self.convert_times_char(query_text, r, 1000000)
        # same as above, but can finishes with EOL
        r = re.compile(r'(\d+\.?\d*)\s?mil\w*$')
        query_text = self.convert_times_char(query_text, r, 1000000)
        return query_text
