import numbers
import re

import numpy as np
import schwifty

from nb_query_eval.ensemble import Evaluator


class EntityRecognizer(Evaluator):
    """
    Entity extractor. Using regex to extract entities from tokens.
    """

    def __init__(self, entity_matrix, currencies, prior_denominator='auto',
                 general_bf=3) -> None:
        """
        Obtain analytically created matrix for naive bayes (NB),
        currencies and their synonyms used in natural language,
        prior denominator (should be number: number of target classes)
        for prior probability of a class match,
        general bayes factor used in NB computation

        Parameters
        ----------
        entity_matrix: pd.DataFrame
            columns: action names, values either nan or 1
            (to be used in naive bayes)
        currencies: pd.DataFrame
            columns: [currency_code,currency_synonym], values as str.
        prior_denominator: int
            Default: 'auto': prior will be 1 / number_of_actions.
            If int, prior will be 1/prior_denominator
        general_bf: int
            Default: 3. Bayes Factor for all naive bayes features
        """
        self.entity_matrix = entity_matrix
        self.currencies = currencies
        if prior_denominator == 'auto':
            self.prior = 1 / len(self.entity_matrix.columns)
        elif isinstance(prior_denominator,
                        numbers.Number) and prior_denominator != 0:
            self.prior = 1 / prior_denominator
        else:
            raise ValueError('prior_denominator should be a nonzero number.')
        self.general_bf = general_bf
        self.tokens = []
        self.entities = {}

    def fit(self, tokens):
        """
        If any entity is present in the query, updates tokens accordingly.

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`
        action_names: list

        Returns
        -------
        list
             updated tokens

        """
        tks = self.fit_account(tokens)
        tks = self.fit_currencies(tks)
        tks = self.fit_iban(tks)

        tks = self.fit_only_one_amount(tks)

        self.tokens = tks
        self.entities = {}
        return tks

    def predict(self):
        """
        Based on entity matrix predict probabilities of actions,
        if any entity is extracted.

        Returns
        -------
        list
            action probabilities
        """
        kw_mat = self.entity_matrix.copy()
        entities = {}
        for tk in self.tokens:
            kw_mat = self.modify_keyword_row('suma', entities, kw_mat, tk)
            kw_mat = self.modify_keyword_row('IBAN', entities, kw_mat, tk)
            kw_mat = self.modify_keyword_row('ucet', entities, kw_mat, tk)

        self.entities = entities.copy()

        posterior = self.prior * np.nanprod(kw_mat, axis=0)
        posterior_corr = posterior / (posterior + 1)
        normalized = [x / sum(posterior_corr) for x in posterior_corr]
        action_probas = normalized
        return action_probas

    def modify_keyword_row(self, ent, entities, kw_mat, tk):
        if tk[1] == ent and entities.get(ent) is None:
            kw_mat.loc[ent] = kw_mat.loc[ent] * self.general_bf
            entities[ent] = tk[0]
        return kw_mat

    def get_entities(self):
        """
        Obtain extracted entities

        Returns
        -------
        dict
            entities
        """
        return self.entities

    def _fit_currencies(self, tokens):
        """
        From NUMeric tokens finds the number (value/amount)
        with corresponding currency.
        Given tokens are not modified, but the modification is returned.

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`

        Returns
        -------
        list
            updated tokens
        """
        # A ked je currency prva?
        tks = list(tokens).copy()
        for i in range(len(tks)):
            if tks[i][1] == 'NUM':
                tks = self._find_num_for_currency(tks, i)
        return [t for t in tks if t != 'INVALID']

    def fit_currencies(self, tokens):
        tokens_fresh = []
        while tokens:
            tk = tokens.pop(0)
            if tk[1] == 'NUM':
                # candidate with currency synonym in one token
                potential_tk = self._amt_curr(tk[0])
                if potential_tk:
                    tokens_fresh.append((potential_tk, 'suma'))
                    continue
                elif tokens:
                    # candidate with currency synonym in next token
                    potential_tk = self._amt_curr(tk[0] + tokens[0][0])
                    if potential_tk:
                        _ = tokens.pop(0)
                        tokens_fresh.append((potential_tk, 'suma'))
                        continue
            tokens_fresh.append(tk)

        return tokens_fresh

    def fit_iban(self, tokens):
        """
        From NUMeric tokens finds IBAN and sets tokens accordingly.

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`

        Returns
        -------
        list
            updated tokens
        """
        tks = tokens.copy()
        for i in range(len(tks)):
            if tks[i][1] == 'NUM':
                tks = self._find_iban_in_tokens(tks, i)
        return [t for t in tks if t[1] != 'INVALID']

    def fit_account(self, tokens):
        """
        From NUMeric tokens finds account number and sets tokens accordingly.

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`

        Returns
        -------
        list
            updated tokens
        """
        tks = tokens.copy()
        for i in range(len(tks)):
            if tks[i][1] == 'NUM':
                tks = self.find_acc_num(tks, i)
        return tks

    def fit_only_one_amount(self, tokens):
        """
        If no currency is next to some number in input,
        extract this number as amount in default currency (CZK)

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`

        Returns
        -------
        list
            updated tokens
        """
        amt = [tk for tk in tokens if tk[1] == 'suma']
        if amt:
            return tokens
        potential_amount = [(i, tk) for i, tk in enumerate(tokens) if
                            tk[1] == 'NUM' and tk[1] != 'suma']
        if len(potential_amount) == 1:
            i, tk = potential_amount[0]
            tks = tokens.copy()
            try:
                num = float(tk[0])
                tks[i] = ((num, 'CZK'), 'suma')
            except ValueError:
                pass
            return tks
        return tokens

    def _find_iban_in_tokens(self, tokens, idx):
        """
        Finds IBAN in tokens and checks its correctness with schwifty lib.

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`
        idx: int
            index of numeric token

        Returns
        -------
        list
            updated tokens

        """
        # 1. candidate: IBAN is written in one token
        try:
            iban = schwifty.IBAN(tokens[idx][0]).compact
            tokens[idx] = (iban, 'IBAN')
            return tokens
        except ValueError:
            pass
        # 2. candidate: country prefix might be separated, look back once
        cand = tokens[idx - 1][0] + tokens[idx][0]
        try:
            iban = schwifty.IBAN(cand).compact
            tokens[idx - 1] = (tokens[idx - 1][0], 'INVALID')
            tokens[idx] = (iban, 'IBAN')
            return tokens
        except ValueError:
            pass
        # 3. candidates: look forward to the end of tokens, maybe one token back
        for i in range(idx + 1, len(tokens)):
            if tokens[i][1] == 'NUM':
                cand += tokens[i][0]
                try:
                    iban = schwifty.IBAN(cand).compact
                    tokens[idx - 1] = (tokens[idx - 1][0], 'INVALID')
                    for j in range(idx + 1, i + 1):
                        tokens[j] = (tokens[j][0], 'INVALID')
                    tokens[idx] = iban, 'IBAN'
                    return tokens
                except ValueError:
                    pass
        return tokens

    def _find_num_for_currency(self, tokens, idx):
        """
        Finds the number (amount/value) for any currency from the currency list.

        Two types of candidates for value & currency:
        1. no space (20Kc, 30eur, "20,-", ...)
        2. with space (20 korun, 30 euro, ...)

        That's why I need an index to look back&forth.

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`
        idx
            position in tokens

        Returns
        -------
        tuple
            * updated token tuple ((amount, currency code), 'suma') if found
            * current token on given index if not found

        """
        if self.currencies is None:
            return tokens

        # 1st candidate is only on one token
        cand1 = tokens[idx][0]
        amount_curr = self._amt_curr(cand1)
        if amount_curr:
            tokens[idx] = amount_curr, 'suma'
            return tokens

        # 2nd candidate may be on consequent tokens (`20kc' VS. `20 kc')
        if idx + 1 < len(tokens):
            cand2 = tokens[idx][0] + tokens[idx + 1][0]
            amount_curr = self._amt_curr(cand2)
            if amount_curr:
                tokens[idx] = amount_curr, 'suma'
                tokens[idx + 1] = 'INVALID'
                return tokens
        return tokens

    def _amt_curr(self, text):
        match = re.match(r'(\d{1,11}\.?\d*)([^\d]+)', text, re.IGNORECASE)
        if match:
            potential_val, potential_curr_syno = match.groups()
            pot_curr_idx = self.currencies['currency_synonym'].str.contains(
                potential_curr_syno, case=False, regex=False)
            results = self.currencies[pot_curr_idx]
            if not results.empty:
                curr = results.iloc[0]['currency_code']
                return float(potential_val), curr
        return None

    def find_acc_num(self, tokens, idx):
        """
        Finds czech account number with or without slash

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`
        idx
            position in tokens

        Returns
        -------
        list
            updated tokens

        """
        # prefix (<=6 digits) - acc. number (up to 10 digits) / bank code (4 d)
        cand = tokens[idx][0]
        m = re.match(r'^\d{0,6}-?\d{2,10}\/\d{4}$', cand)
        if m:
            tokens[idx] = (tokens[idx][0]), 'ucet'
            return tokens
        return tokens
