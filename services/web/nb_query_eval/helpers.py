import pandas as pd


def read_stop_words(stop_words_path):
    """
    Reads stopwords from path - one SW per line - for tokenization.

    Parameters
    ----------
    stop_words_path: str

    Returns
    -------
    list
        stopwords in str
    """
    if stop_words_path:
        with open(str(stop_words_path)) as f:
            return [line.strip() for line in f.readlines()]
    raise RuntimeError('No stop_words_path in recipe')


def read_currencies(currencies_path) -> pd.DataFrame:
    """
    Reads the currency list from given path.

    Parameters
    ----------
    currencies_path: str, Path

    Returns
    -------
    pandas.DataFrame
        Empty if no currencies path given.

    """
    if currencies_path:
        return pd.read_csv(str(currencies_path))
    raise RuntimeError('No currencies_path in recipe')


def read_keyword_matrix(keyword_matrix_path) -> pd.DataFrame:
    """
    Reads the keyword matrix for NB classification

    Parameters
    ----------
    keyword_matrix_path: str, Path

    Returns
    -------
    pandas.DataFrame
    """
    if keyword_matrix_path:
        km = pd.read_csv(str(keyword_matrix_path), index_col=0)
        km = km.sort_index(axis='columns')
        return km
    raise RuntimeError('No keyword_matrix_path in recipe')


def read_entity_matrix(entity_matrix_path) -> pd.DataFrame:
    """
    Reads the entity matrix for query classification

    Parameters
    ----------
    entity_matrix_path: str

    Returns
    -------
    pandas.Dataframe
    """
    if entity_matrix_path:
        em = pd.read_csv(str(entity_matrix_path), index_col=0)
        em = em.sort_index(axis='columns')
        return em
    raise RuntimeError('No entity_matrix_path')
