import pytest


@pytest.mark.parametrize(['query', 'tokens_exp'], [
    ('potrebuji poslat 100 korun nebo 100korun nebo 100 na ucet SK48...',
     [('potrebuji', 'KW'), ('poslat', 'KW'), ('100', 'NUM'), ('korun', 'KW'),
      ('nebo', 'SW'), ('100korun', 'NUM'),
      ('nebo', 'SW'), ('100', 'NUM'), ('na', 'SW'), ('ucet', 'KW'),
      ('sk48', 'NUM'), ('...', 'NKW')]),
    ('skus... co?! platba&posta, za 20% urok5€',
     [('skus', 'KW'), ('...', 'NKW'), ('co', 'SW'), ('?', 'NKW'), ('!', 'NKW'),
      ('platba', 'KW'), ('&', 'NKW'),
      ('posta', 'KW'), (',', 'NKW'), ('za', 'SW'), ('20', 'NUM'), ('%', 'NKW'),
      ('urok5€', 'NUM')]),
    ('slovo91530!!!',
     [('slovo91530', 'NUM'), ('!', 'NKW'), ('!', 'NKW'), ('!', 'NKW')]),
    ('Zaplať 250´000,- hypo',
     [('zaplať', 'KW'), ('250000czk', 'NUM'), ('hypo', 'KW')])
])
def test_tokenizer(tokenizer, query, tokens_exp):
    tokens_act = tokenizer.fit(query)
    assert tokens_act == tokens_exp


@pytest.mark.parametrize(['query', 'tokens'], [
    ('000 kč', [('000', 'NUM'), ('kč', 'KW'), ]),
    ('100 000', [('100000', 'NUM')]),
    ('100`000', [('100000', 'NUM')]),
    ('100´000,-', [('100000czk', 'NUM')]),
    ('100\'000', [('100000', 'NUM')]),
    ('100,000', [('100.000', 'NUM')]),
    ('100.00', [('100.00', 'NUM')]),
    ('100k', [('100000', 'NUM')]),
    ('1m', [('1000000', 'NUM')]),
    ('1M', [('1000000', 'NUM')]),
    ('1,2M', [('1200000', 'NUM')]),
    ('1mil', [('1000000', 'NUM')]),
    ('1mil.', [('1000000', 'NUM')]),
])
def test_experiment_tokens(tokenizer, query, tokens):
    tks = tokenizer.fit(query)
    assert tks == tokens


def test_tokenizer_preprocess(tokenizer):
    q = '89 37 04 00 44 05 32 01 30 00 posli 10eur'
    q_a = tokenizer.preprocess(q)
    assert q_a == '89370400440532013000 posli 10eur'


@pytest.mark.parametrize(['query', 'preprocessed'], [
    ('posli 1.2k na', 'posli 1200 na'),
    ('posli 1.2korun', 'posli 1.2korun'),
    ('posli 1.2korun na', 'posli 1.2korun na'),
    ('posli 1.2k', 'posli 1200 '),
])
def test_tokenizer_thousands(tokenizer, query, preprocessed):
    q_a = tokenizer.preprocess(query)
    assert q_a == preprocessed
