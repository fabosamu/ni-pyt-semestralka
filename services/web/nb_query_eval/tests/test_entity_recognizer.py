import pytest


@pytest.mark.parametrize('query', (
        '100CZK na DE89370400440532013000',
        'posli 100 CZK na DE89370400440532013000',
        'posli 100czk na DE89370400440532013000 a nejaky bordel na konci',
        'posli 100czk na DE89370400440532013000 a babicke 200czk na jej ucet.',
        'posli 100euricok na DE89370400440532013000 a babicke 100czk.',
        'posli 200korun na DE89370400440532013000 a babicke 100czk na jej ucet',
        'posli 100 korun na ucet babicky',
        'posli 100,- na ucet babicky',
        'posli 100 na ucet babicky',
        'posli 100 korun',
        'posli 100,-',
        '100czk',
))
def test_contains_currency_czk_and_amount(entity_recognizer, tokenizer, query):
    tokens = tokenizer.fit(query)
    tokens = entity_recognizer.fit_currencies(tokens)
    assert ((100, 'CZK'), 'suma') in tokens


@pytest.mark.parametrize(['query', 'entities'], [
    ('posli 000 kč na bla', {'suma': (0, 'CZK')}),
    ('posli 100 000 czk', {'suma': (100000, 'CZK')}),
    ('posli 100 000 czk', {'suma': (100000, 'CZK')}),
    ('posli 100\'000 czk', {'suma': (100000, 'CZK')}),
    ('posli 100`000 czk', {'suma': (100000, 'CZK')}),
    ('posli 100´000 czk', {'suma': (100000, 'CZK')}),
    ('posli 100,000 czk', {'suma': (100, 'CZK')}),
    ('posli 100000,-', {'suma': (100000, 'CZK')}),
    ('posli 100k na bla', {'suma': (100000, 'CZK')}),
    ('posli 100K na bla', {'suma': (100000, 'CZK')}),
    ('posli 1m korun na bla', {'suma': (1000000, 'CZK')}),
    ('posli 1M na bla', {'suma': (1000000, 'CZK')}),
    ('posli 1,2M na bla', {'suma': (1200000, 'CZK')}),
    ('posli 1mil korun na bla', {'suma': (1000000, 'CZK')}),
    ('posli 1mil. korun na bla', {'suma': (1000000, 'CZK')}),
    ('posli 10 milionů korun na bla', {'suma': (10000000, 'CZK')}),
    ('posli 1 milion korun na bla', {'suma': (1000000, 'CZK')}),
    ('posli 2miliony korun na bla', {'suma': (2000000, 'CZK')}),
])
def test_various_types_amt(entity_recognizer, tokenizer, query, entities):
    tokens = tokenizer.fit(query)
    print(tokens)
    tks = entity_recognizer.fit(tokens)
    print('after ner:', tks)
    _ = entity_recognizer.predict()
    assert entity_recognizer.entities == entities


@pytest.mark.parametrize(['query', 'entities'], [
    ('posli 1e8 korun na bla', {'suma': (100000000, 'CZK')}),
    ('ucet 12e123/132e3', {}),
])
def test_e_notation(entity_recognizer, tokenizer, query, entities):
    tokens = tokenizer.fit(query)
    tks = entity_recognizer.fit(tokens)
    print('after ner:', tks)
    _ = entity_recognizer.predict()
    assert entity_recognizer.entities == entities


@pytest.mark.parametrize('query', (
        '100DAN na DE89370400440532013000',
        'posli 100 seklov na DE89370400440532013000',
        'posli 100dinarov na DE89370400440532013000 a nejaky bordel na konci',
        'posli 100 fazuliek na ucet babicky',
        'posli 100/ na ucet babicky',
        'chci poslat 2000 nekomu na ucet',
        'posli 100',
        '100fazul',
        '100 fazul',
        '1+1',
        'poslat matce 100+100 korun',
))
def test_not_contains_currency_czk_and_amt(entity_recognizer, tokenizer, query):
    tokens = tokenizer.fit(query)
    tokens = entity_recognizer.fit_currencies(tokens)
    assert 'suma' not in [tk[1] for tk in tokens]


def test_fit_currencies_correct(tokenizer, entity_recognizer):
    w = '20kc 20 kc 20 korun DE1023 0403 0340 3045 4343'
    tokens = tokenizer.fit(w)
    expected_tokens = [
        ((20, 'CZK'), 'suma'), ((20, 'CZK'), 'suma'), ((20, 'CZK'), 'suma'),
        ('de10230403034030454343', 'NUM')]
    assert entity_recognizer.fit_currencies(tokens) == expected_tokens


def test_fit_currencies_correct_eur(tokenizer, entity_recognizer):
    w = 'posli 10 korun na DX 89370400440532013000 opakovane'
    tokens = tokenizer.fit(w)
    expected_tokens = [
        ('posli', 'KW'), ((10, 'CZK'), 'suma'), ('na', 'SW'),
        ('dx', 'KW'), ('89370400440532013000', 'NUM'), ('opakovane', 'KW')]
    assert expected_tokens == entity_recognizer.fit_currencies(tokens)


@pytest.mark.parametrize(
    ['query', 'has_iban'],
    [('posli 10e na DE89370400440532013000', True),
     ('posli 10e na DE 89370400440532013000 opakovane', True),
     ('posli 10e na DE 8937 0400 4405 3201 3000 jednorazovo', True),
     ('posli 10e na DE 89 37 04 00 44 05 32 01 30 00 nejak inak whatever',
      True),
     ('posli na DE 89 37 04 00 44 05 32 01 30 00 castku 10eur alebo czk', True),
     ('', False),
     ('posli 10e na DX 89370400440532013000 opakovane', False),
     ('posli 10e na D 8937 0400 4405 3201 3000 jednorazovo', False),
     ('posli 10e na AS 89 37 04 00 44 05 32 01 30 00 nejak inak whatever',
      False),
     ('posli na  89 37 04 00 44 05 32 01 30 00 castku 10eur alebo czk', False),
     ]
)
def test_contains_iban(entity_recognizer, tokenizer, query, has_iban):
    tokens = tokenizer.fit(query)
    tokens = entity_recognizer.fit_iban(tokens)
    _ = entity_recognizer.predict()
    if has_iban:
        assert ('DE89370400440532013000', 'IBAN') in tokens
    else:
        assert 'IBAN' not in [tk[1] for tk in tokens]


@pytest.mark.parametrize(['query', 'tokens_exp'], [
    ('posli 10e na DE89370400440532013000',
     [('posli', 'KW'), ('10e', 'NUM'), ('na', 'SW'),
      ('DE89370400440532013000', 'IBAN')]),
    ('posli 10e na DE 89370400440532013000 opakovane',
     [('posli', 'KW'), ('10e', 'NUM'), ('na', 'SW'),
      ('DE89370400440532013000', 'IBAN'), ('opakovane', 'KW')]),
    ('posli 10e na DE 8937 0400 4405 3201 3000 jednorazovo',
     [('posli', 'KW'), ('10e', 'NUM'), ('na', 'SW'),
      ('DE89370400440532013000', 'IBAN'), ('jednorazovo', 'KW')]),
    ('posli 10e na DE 89 37 04 00 44 05 32 01 30 00 nejak inak whatever',
     [('posli', 'KW'), ('10e', 'NUM'), ('na', 'SW'),
      ('DE89370400440532013000', 'IBAN'), ('nejak', 'KW'),
      ('inak', 'KW'), ('whatever', 'KW')]),
    ('posli na DE 89 37 04 00 44 05 32 01 30 00 ciastku 10eur alebo czk',
     [('posli', 'KW'), ('na', 'SW'),
      ('DE89370400440532013000', 'IBAN'), ('ciastku', 'KW'), ('10eur', 'NUM'),
      ('alebo', 'KW'), ('czk', 'KW')]),
])
def test_set_more_ibans_correctly(tokenizer, entity_recognizer, query,
                                  tokens_exp):
    tokens = tokenizer.fit(query)
    tokens_act = entity_recognizer.fit_iban(tokens)
    assert tokens_act == tokens_exp


@pytest.mark.parametrize('query', [
    '',
    'posli 10e na DX 89370400440532013000 opakovane',
    'posli 10e na D 8937 0400 4405 3201 3000 jednorazovo',
    'posli 10e na AS 89 37 04 00 44 05 32 01 30 00 nejak inak whatever',
    'posli na  89 37 04 00 44 05 32 01 30 00 10eur alebo czk',
])
#     TODO: fix iban and amount issue somehow
def test_no_set_ibans(tokenizer, entity_recognizer, query):
    tokens = tokenizer.fit(query)
    tks = entity_recognizer.fit_iban(tokens)
    assert tks == tokens


@pytest.mark.parametrize(['query', 'tokens_exp'], [
    ('ucet 19-2000145399/0800',
     [('ucet', 'KW'), ('19-2000145399/0800', 'ucet')]),
    ('ucet 178124-4159/0710',
     [('ucet', 'KW'), ('178124-4159/0710', 'ucet')]),
])
def test_account_number(tokenizer, entity_recognizer, query, tokens_exp):
    tokens = tokenizer.fit(query)
    tokens_act = entity_recognizer.fit_account(tokens)
    assert tokens_act == tokens_exp


@pytest.mark.parametrize(['query', 'tokens_exp'], [
    ('posli 500 mame',
     [('posli', 'KW'), ((500, 'CZK'), 'suma'), ('mame', 'KW')]),
    ('posli 500 mame a 200 starkej',
     [('posli', 'KW'), ('500', 'NUM'), ('mame', 'KW'), ('a', 'SW'),
      ('200', 'NUM'), ('starkej', 'KW')]),
])
def test_only_one_amount(tokenizer, entity_recognizer, query, tokens_exp):
    tokens = tokenizer.fit(query)
    tokens_act = entity_recognizer.fit_only_one_amount(tokens)
    assert tokens_act == tokens_exp


@pytest.mark.parametrize(['query', 'entities_exp'], [
    ('posli 500 korun mame na 12345678/2010 ',
     {'suma': (500, 'CZK'), 'ucet': '12345678/2010'}),
    ('nic nerob proste', {}),
    ('IBAN SK48 0200 0000 0031 1906 4056 kam chcem 5000 eur kazdy den',
     {'suma': (5000, 'EUR'), 'IBAN': 'SK4802000000003119064056'}),
    ('20,- na ucet 19-2000145399/0800',
     {'ucet': '19-2000145399/0800', 'suma': (20, 'CZK')}),
    ('ucet 178124-4159/0710',
     {'ucet': '178124-4159/0710'}),
    ('ucet 4544332159/0710',
     {'ucet': '4544332159/0710'}),
    ('ucet 12e123/132e3',
     {}),
    ('100kč na 123456789/0100 roku 2021',
     {'ucet': '123456789/0100', 'suma': (100, 'CZK')}),
])
def test_extracts_entities(entity_recognizer, tokenizer, query, entities_exp):
    entity_recognizer.fit(tokenizer.fit(query))
    _ = entity_recognizer.predict()
    entities_act = entity_recognizer.get_entities()
    assert entities_act == entities_exp
