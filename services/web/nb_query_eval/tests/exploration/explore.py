from pathlib import Path

import pandas as pd

cur_dir = Path.cwd()
root_dir = cur_dir / '..' / '..'

if __name__ == "__main__":
    data = pd.read_csv(str(cur_dir / 'data/query_intents-2021-02-03.csv'))
    zv = data[['dotaz', 'zvolena_akce']].dropna().rename(
        columns={'zvolena_akce': 'class'})
    vy = data[['dotaz', 'vybrana_akce']].dropna().rename(
        columns={'vybrana_akce': 'class'})
    data = pd.concat([zv, vy], axis=0)
    gb = data.groupby('class').count()
    print(gb)
