###########
# BUILDER #
###########

# pull official base image
# FROM python:3.8.1-slim-buster as builder

# # set work directory
# WORKDIR /usr/src/app

# # set environment variables
# ENV PYTHONDONTWRITEBYTECODE 1
# ENV PYTHONUNBUFFERED 1

# # install system dependencies
# RUN apt-get update && \
#     apt-get install -y --no-install-recommends gcc && \
#     apt-get install -y dos2unix

# # lint
# RUN pip install --upgrade pip
# RUN pip install flake8
# RUN pip install tox
# COPY . /usr/src/app/
# # RUN flake8 --ignore=E501,F401,E126,W605 .
# # RUN tox

# # install python dependencies
# COPY ./requirements.txt .
# RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt


#########
# FINAL #
#########

# pull official base image
FROM python:3.8.1-slim-buster

# create directory for the app user
RUN mkdir -p /home/app
RUN mkdir -p /home/app/nltk_data

# create the app user
RUN addgroup --system app && adduser --system app --ingroup app

# create the appropriate directories
ENV HOME=/home/app
ENV APP_HOME=/home/app/web
ENV NLTK_HOME=/home/app/nltk_data
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# install dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends netcat gcc && \
    apt-get install -y dos2unix

# COPY --from=builder /usr/src/app/wheels /wheels
COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN [ "python", "-c", "import nltk; nltk.download('punkt')" ]


COPY . /usr/src/app/

# copy entrypoint.sh
COPY ./entrypoint.sh $APP_HOME

# copy project
COPY . $APP_HOME

# chown all the files to the app user
RUN chown -R app:app $APP_HOME
RUN chown -R app:app $NLTK_HOME

# change to the app user
USER app

# RUN apt-get install -y dos2unix

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh || true
# RUN dos2unix /entrypoint.sh

# run entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]